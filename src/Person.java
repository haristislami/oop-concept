class Person { //class
    String name;
    Person(){
    }
    Person(String name){
        this.name = name;
    }

    void sayHello(String name){
        System.out.println("Hallo " + name + ", I am " + this.name); //"this" keyword is to avoid variable shadowing
    }
}