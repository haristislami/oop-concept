class Trainer extends Person { // inheritance
    Trainer(String name){
        super(name);
    }

    //Method Overriding
    @Override
    void sayHello(String name){
        System.out.println("Hello " + name + ", I am " + this.name + " new trainer");
    }

}